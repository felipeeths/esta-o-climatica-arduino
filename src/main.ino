#include <OneWire.h>
#include <DallasTemperature.h>
#include "DHT.h"
#include <ESP8266WiFi.h> // Importa a Biblioteca ESP8266WiFi
#include <PubSubClient.h> // Importa a Biblioteca PubSubClient

#define DHTTYPE DHT11 // DHT 11
#define leSensorChuva_HumidadeTerra A0

#define ligaSensorChuva 12
#define ligaSensorHumidadeTerra 13
#define ligaSensorHumidadeAr 14
#define ONE_WIRE_BUS 4  // Porta do pino de sinal do DS18B20 Temperatura.

// Define uma instancia do oneWire para comunicacao com o sensor
OneWire oneWire(ONE_WIRE_BUS);

//DHT 11
DHT dht(ligaSensorHumidadeAr, DHTTYPE);
// Armazena temperaturas minima e maxima
float tempMin = 999;
float tempMax = 0;

DallasTemperature sensors(&oneWire);
DeviceAddress sensor1;

#define TOPICO_SUBSCRIBE "felipeenvia"     //tópico MQTT de escuta
#define TOPICO_PUBLISH "feliperecebe"

#define ID_MQTT  "estacaoiotfelipe"

const char* SSID = "CENTRO MAKER"; // SSID nome da rede WI-FI que deseja se conectar
const char* PASSWORD = "revolucionarios"; // Senha da rede WI-FI que deseja se conectar

// MQTT
const char* BROKER_MQTT = "iot.eclipse.org"; //URL do broker MQTT que se deseja utilizar
int BROKER_PORT = 1883; // Porta do Broker MQTT

WiFiClient espClient; // Cria o objeto espClient
PubSubClient MQTT(espClient); // Instancia o Cliente MQTT passando o objeto espClient

String mensagem = "";
// Inicializa o LCD

void setup(void)
{
  pinMode(leSensorChuva_HumidadeTerra, INPUT);
  pinMode(ligaSensorChuva, OUTPUT);
  pinMode(ligaSensorHumidadeTerra, OUTPUT);
  pinMode(ligaSensorHumidadeAr, OUTPUT);
  Serial.begin(9600);
  dht.begin();
  sensors.begin();
  InitOutput();
  initWiFi();
  initMQTT();
  // Localiza e mostra enderecos dos sensores
  Serial.println("Localizando sensores DS18B20...");
  Serial.print("Foram encontrados ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.println(" sensores.");
  if (!sensors.getAddress(sensor1, 0))
     Serial.println("Sensores nao encontrados !");
  // Mostra o endereco do sensor encontrado no barramento
  Serial.print("Endereco sensor: ");
  mostra_endereco_sensor(sensor1);
  Serial.println();
  Serial.println();
}

void mostra_endereco_sensor(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // Adiciona zeros se necessário
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

void loop()
{
  // Le a informacao do sensor
  sensors.requestTemperatures();
  float tempC = sensors.getTempC(sensor1);
  // Mostra dados no serial monitor
  Serial.print("Sensor de Temperatura : ");
  Serial.println(tempC);
  mensagem = tempC;
  mensagem += " T";
  VerificaConexoesWiFIEMQTT();
  EnviaEstadoOutputMQTT();
  digitalWrite(ligaSensorChuva, HIGH);
  delay(500);
  float sensor = analogRead(leSensorChuva_HumidadeTerra);
  Serial.print("Sensor de chuva : ");
  Serial.print(sensor);
  mensagem = sensor;
  mensagem += " C";
  VerificaConexoesWiFIEMQTT();
  EnviaEstadoOutputMQTT();
  Serial.print("\n");
  digitalWrite(ligaSensorChuva, LOW);
  digitalWrite(ligaSensorHumidadeTerra, HIGH);
  delay(500);
  sensor = analogRead(leSensorChuva_HumidadeTerra);
  Serial.print("Sensor de Humidade da terra : ");
  Serial.print(sensor);
  mensagem = sensor;
  mensagem += " HT";
  VerificaConexoesWiFIEMQTT();
  EnviaEstadoOutputMQTT();
  Serial.print("\n");
  digitalWrite(ligaSensorHumidadeTerra, LOW);
  delay(500);
  float h = dht.readHumidity();
  Serial.print("Sensor de Humidade do Ar : ");
  Serial.print(h);
  mensagem = h;
  mensagem += " HA";
  VerificaConexoesWiFIEMQTT();
  EnviaEstadoOutputMQTT();
  Serial.print(" %");
  Serial.print("\n");
  delay(500);
}


void initMQTT()
{
    MQTT.setServer(BROKER_MQTT, BROKER_PORT);   //informa qual broker e porta deve ser conectado
    MQTT.setCallback(mqtt_callback);            //atribui função de callback (função chamada quando qualquer informação de um dos tópicos subescritos chega)
}

//Função: função de callback
//        esta função é chamada toda vez que uma informação de
//        um dos tópicos subescritos chega)
//Parâmetros: nenhum
//Retorno: nenhum
void mqtt_callback(char* topic, byte* payload, unsigned int length){
    String msg;
    //obtem a string do payload recebido
    for(int i = 0; i < length; i++){
       char c = (char)payload[i];
       msg += c;
    }
    //toma ação dependendo da string recebida:
    //verifica se deve colocar nivel alto de tensão na saída D0:
    //IMPORTANTE: o Led já contido na placa é acionado com lógica invertida (ou seja,
    //enviar HIGH para o output faz o Led apagar / enviar LOW faz o Led acender)
    if (msg.equals("L")) {
      //  digitalWrite(relay, HIGH);
        //EstadoSaida = '1';
    }
    //verifica se deve colocar nivel alto de tensão na saída D0:
    if (msg.equals("D")){
      //  digitalWrite(relay, LOW);
        //EstadoSaida = '0';
    }

}

//Função: reconecta-se ao broker MQTT (caso ainda não esteja conectado ou em caso de a conexão cair)
//        em caso de sucesso na conexão ou reconexão, o subscribe dos tópicos é refeito.
//Parâmetros: nenhum
//Retorno: nenhum
void reconnectMQTT(){
    while (!MQTT.connected()){
    //    Serial.print("* Tentando se conectar ao Broker MQTT: ");
  //      Serial.println(BROKER_MQTT);
        if (MQTT.connect(ID_MQTT)){
      //      Serial.println("Conectado com sucesso ao broker MQTT!");
            MQTT.subscribe(TOPICO_SUBSCRIBE);
        }
        else {
        //    Serial.println("Falha ao reconectar no broker.");
        //    Serial.println("Havera nova tentatica de conexao em 2s");
            delay(2000);
        }
    }
}

//Função: inicializa e conecta-se na rede WI-FI desejada
//Parâmetros: nenhum
//Retorno: nenhum
void initWiFi()
{
    delay(10);
//    Serial.println("------Conexao WI-FI------");
//    Serial.print("Conectando-se na rede: ");
//    Serial.println(SSID);
  //  Serial.println("Aguarde");
    reconectWiFi();
}
//Função: reconecta-se ao WiFi
//Parâmetros: nenhum
//Retorno: nenhum
void reconectWiFi(){
    //se já está conectado a rede WI-FI, nada é feito.
    //Caso contrário, são efetuadas tentativas de conexão
    if (WiFi.status() == WL_CONNECTED)
        return;

    WiFi.begin(SSID, PASSWORD); // Conecta na rede WI-FI

    while (WiFi.status() != WL_CONNECTED){
        delay(100);
      //  Serial.print(".");
    }
  //  Serial.println();
  //Serial.print("Conectado com sucesso na rede ");
//    Serial.print(SSID);
  //  Serial.println("IP obtido: ");
  //  Serial.println(WiFi.localIP());
}

void VerificaConexoesWiFIEMQTT(void){
    if (!MQTT.connected())
        reconnectMQTT(); //se não há conexão com o Broker, a conexão é refeita

     reconectWiFi(); //se não há conexão com o WiFI, a conexão é refeita
}

//Função: envia ao Broker o estado atual do output
//Parâmetros: nenhum
//Retorno: nenhum
void EnviaEstadoOutputMQTT(void){
    MQTT.publish(TOPICO_PUBLISH,mensagem.c_str());
    Serial.println("- Estado da saida D0 enviado ao broker: ");
    Serial.println(mensagem);
    delay(2000);
}

//Função: inicializa o output em nível lógico baixo
//Parâmetros: nenhum
//Retorno: nenhum
void InitOutput(void){
    //IMPORTANTE: o Led já contido na placa é acionado com lógica invertida (ou seja,
    //enviar HIGH para o output faz o Led apagar / enviar LOW faz o Led acender)
    //pinMode(relay, OUTPUT);
  //  digitalWrite(relay, LOW);
}
